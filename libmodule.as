/*
 * file: libmodule.as
 *
 * Common Module Library.
 */
/*
 * About: MIT License
 *
 * Copyright (c) 2021 Anggara Yama Putra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/*
 * namespace: AN
 *
 * --- C++
 * namespace AN {...}
 * ---
 */
namespace AN{

/*
 * obj: g_LibModule
 *
 * Global instance of <AN::LIBMODULE::CLibModule>.
 */
const AN::LIBMODULE::CLibModule g_LibModule();

/*
 * namespace: AN::LIBMODULE
 *
 * --- C++
 * namespace AN::LIBMODULE {...}
 * ---
 */
namespace LIBMODULE{

/*
 * string: _VERSION
 */
const string _VERSION = "2021-03-24.0001";

/*
 * obj: g_LibDebug
 *
 * Global instance of <AN::LIBDEBUG::CLibDebug>.
 */
const AN::LIBDEBUG::CLibDebug g_LibDebug( "Common.Module." + AN::LIBDEBUG::_VERSION );

/*
 * class: AN::LIBMODULE::CLibModule
 *
 * --- C++
 * final class AN::LIBMODULE::CLibModule {...}
 * ---
 *
 * Module library.
 */
final class CLibModule
{

    /*
     * constructor: CLibModule
     *
     * Default constructor.
     */
    CLibModule ()
    {
    }

    /*
     * method: IsPlugin
     *
     * Check whether the script is loaded as plugin.
     *
     * Returns:
     * true     - Is a plugin script.
     * false    - Is a map script.
     */
    bool IsPlugin () const
    {
        return g_Module.GetModuleName() != "MapModule";
    }

}

}}
