/*
 * file: libdebug.as
 *
 * Common Debug Library.
 */
/*
 * About: MIT License
 *
 * Copyright (c) 2021 Anggara Yama Putra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/*
 * namespace: AN
 *
 * --- C++
 * namespace AN {...}
 * ---
 */
namespace AN{

/*
 * namespace: AN::LIBDEBUG
 *
 * --- C++
 * namespace AN::LIBDEBUG {...}
 * ---
 */
namespace LIBDEBUG{

/*
 * string: _VERSION
 */
const string _VERSION = "2021-03-24.0001";

/*
 * class: AN::LIBDEBUG::CLibDebug
 *
 * --- C++
 * final class AN::LIBDEBUG::CLibDebug {...}
 * ---
 *
 * Debugging utilities.
 */
final class CLibDebug
{

    /*
     * constructor: CLibDebug
     *
     * Default constructor.
     */
    CLibDebug ()
    {
    }

    /*
     * constructor: CLibDebug
     *
     * Default constructor.
     *
     * Arguments:
     * szIdentity   - Script identity.
     */
    CLibDebug (const string &in szIdentity)
    {
        this.m_szIdentity   = szIdentity;
    }

    /*
     * method: IsEnabled
     *
     * Check whether the debug mode is enable.
     *
     * Returns:
     * true     - CVar `developer` value is more than 0.
     * false    - Otherwise, return `false`.
     */
    bool IsEnabled () const
    {
        return int( g_EngineFuncs.CVarGetPointer("developer").value ) > 0;
    }

    /*
     * method: IsVerbose
     *
     * Check whether the debug mode is verbose mode.
     *
     * Returns:
     * true     - CVar `developer` value is more than 1.
     * false    - Otherwise, return `false`.
     */
    bool IsVerbose () const
    {
        return int( g_EngineFuncs.CVarGetPointer("developer").value ) > 1;
    }

    /*
     * method: print
     *
     * Print a message to console.
     *
     * Arguments:
     * szMsg    - The message.
     * bForce   - Ignore enabled status.
     */
    void print (const string &in szMsg, const bool bForce = false) const
    {
        if (IsEnabled() || bForce) g_Game.AlertMessage( m_iPrintType, m_szPrintPrefix + szMsg );
    }

    /*
     * method: print
     *
     * Print a message to player console.
     *
     * Arguments:
     * pTargetPlayer    - Target player.
     * szMsg            - The message.
     * bForce           - Ignore enabled status.
     */
    void print (CBasePlayer@ pTargetPlayer, const string &in szMsg, const bool bForce = false) const
    {
        if (IsEnabled() || bForce) g_PlayerFuncs.ClientPrint( pTargetPlayer, m_iPlayerPrintType, m_szPrintPrefix + szMsg );
    }

    /*
     * method: println
     *
     * Print a message to console with newline at the end of the message.
     *
     * Arguments:
     * szMsg    - The message.
     * bForce   - Ignore enabled status.
     */
    void println (const string &in szMsg, const bool bForce = false) const
    {
        print( szMsg + "\n", bForce );
    }

    /*
     * method: println
     *
     * Print a message to player console with newline at the end of the message.
     *
     * Arguments:
     * pTargetPlayer    - Target player.
     * szMsg            - The message.
     * bForce           - Ignore enabled status.
     */
    void println (CBasePlayer@ pTargetPlayer, const string &in szMsg, const bool bForce = false) const
    {
        print( pTargetPlayer, szMsg + "\n", bForce );
    }

    /*
     * method: log
     *
     * Log a message to log file.
     *
     * Arguments:
     * szMsg    - The message.
     * bForce   - Ignore enabled status.
     *
     * Returns:
     * true     - If succeed.
     * false    - Otherwise, return `false`.
     */
    bool log (const string &in szMsg, const bool bForce = false) const
    {
        return (IsEnabled() || bForce) && g_Log.PrintF( m_szLogPrefix + szMsg );
    }

    /*
     * method: logln
     *
     * Log a message to log file with newline at the end of the message.
     *
     * Arguments:
     * szMsg    - The message.
     * bForce   - Ignore enabled status.
     *
     * Returns:
     * true     - If succeed.
     * false    - Otherwise, return `false`.
     */
    bool logln (const string &in szMsg, const bool bForce = false) const
    {
        return log( szMsg + "\n", bForce );
    }

    /*
     * method: verboseprint
     *
     * Print a verbose message to console.
     *
     * Arguments:
     * szMsg    - The message.
     */
    void verboseprint (const string &in szMsg) const
    {
        if (!IsVerbose()) return;
        print( szMsg );
    }

    /*
     * method: verboseprint
     *
     * Print a verbose message to player console.
     *
     * Arguments:
     * pTargetPlayer    - Target player.
     * szMsg            - The message.
     */
    void verboseprint (CBasePlayer@ pTargetPlayer, const string &in szMsg) const
    {
        if (!IsVerbose()) return;
        print( @pTargetPlayer, szMsg );
    }

    /*
     * method: verboseprintln
     *
     * Print a verbose message to console with newline at the end of the message.
     *
     * Arguments:
     * szMsg    - The message.
     */
    void verboseprintln (const string &in szMsg) const
    {
        verboseprint( szMsg + "\n" );
    }

    /*
     * method: verboseprintln
     *
     * Print a verbose message to player console with newline at the end of the message.
     *
     * Arguments:
     * pTargetPlayer    - Target player.
     * szMsg            - The message.
     */
    void verboseprintln (CBasePlayer@ pTargetPlayer, const string &in szMsg) const
    {
        verboseprint( @pTargetPlayer, szMsg + "\n" );
    }

    /*
     * method: verboselog
     *
     * Log a message to log file, if verbose mode enabled.
     *
     * Arguments:
     * szMsg    - The message.
     *
     * Returns:
     * true     - If succeed.
     * false    - Otherwise, return `false`.
     */
    bool verboselog (const string &in szMsg) const
    {
        if (!IsVerbose()) return false;
        return log( szMsg );
    }

    /*
     * method: verboselogln
     *
     * Log a message to log file with newline at the end of the message, if verbose mode enabled.
     *
     * Arguments:
     * szMsg    - The message.
     *
     * Returns:
     * true     - If succeed.
     * false    - Otherwise, return `false`.
     */
    bool verboselogln (const string &in szMsg) const
    {
        return verboselog( szMsg + "\n" );
    }

    /*
     * method: get_m_szPrintPrefix
     *
     * Get the print prefix string.
     *
     * Returns:
     * string   - The print prefix string.
     */
    string get_m_szPrintPrefix () const
    {
        return "Angelscript" + m_szLogPrefix;
    }

    /*
     * method: get_m_szLogPrefix
     *
     * Get the logging prefix string.
     *
     * Returns:
     * string   - The logging prefix string.
     */
    string get_m_szLogPrefix () const
    {
        return "("+g_Module.GetModuleName()+")("+m_szIdentity+"): ";
    }

    /*
     * int: m_iPrintType
     *
     * Debug print type.
     */
    ALERT_TYPE m_iPrintType = at_console;

    /*
     * int: m_iPlayerPrintType
     *
     * Debug print type.
     */
    HUD m_iPlayerPrintType  = HUD_PRINTCONSOLE;

    /*
     * string: m_szIdentity
     *
     * Identity string.
     */
    string m_szIdentity;

}

}}
