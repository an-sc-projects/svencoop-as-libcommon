/*
 * file: libbit.as
 *
 * Common Bit Twiddling Library.
 *
 * Some methods are taken from Bit Twiddling Hacks By Sean Eron Anderson.
 * <https://graphics.stanford.edu/~seander/bithacks.html>
 */
/*
 * About: MIT License
 *
 * Copyright (c) 2021 Anggara Yama Putra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/*
 * namespace: AN
 *
 * --- C++
 * namespace AN {...}
 * ---
 */
namespace AN{

/*
 * obj: g_LibBit
 *
 * Global instance of <AN::LIBBIT::CLibBit>.
 */
const AN::LIBBIT::CLibBit g_LibBit();

/*
 * namespace: AN::LIBBIT
 *
 * --- C++
 * namespace AN::LIBBIT {...}
 * ---
 */
namespace LIBBIT{

/*
 * string: _VERSION
 */
const string _VERSION = "2021-03-26.0002";

/*
 * string: FIRST_ALPHABET
 */
const char FIRST_ALPHABET = 'a';

/*
 * string: LAST_ALPHABET
 */
const char LAST_ALPHABET = 'z';

/*
 * string: ALPHABET_COUNT
 */
const uint ALPHABET_COUNT = uint(LAST_ALPHABET) - FIRST_ALPHABET + 1;

/*
 * obj: g_LibDebug
 *
 * Global instance of <AN::LIBDEBUG::CLibDebug>.
 */
const AN::LIBDEBUG::CLibDebug g_LibDebug( "Common.Bit." + AN::LIBDEBUG::_VERSION );

/*
 * class: AN::LIBBIT::CLibBit
 *
 * --- C++
 * final class AN::LIBBIT::CLibBit {...}
 * ---
 *
 * Bit Twiddling Library.
 */
final class CLibBit
{

    /*
     * constructor: CLibBit
     *
     * Default constructor.
     */
    CLibBit ()
    {
    }

    /*
     * method: IsPowerOfTwo
     *
     * Check whether this number is power of two.
     *
     * Arguments:
     * uiInput      - Input number to check.
     *
     * Returns:
     * true     - Value is power of two.
     * false    - Value is not power of two.
     */
    bool IsPowerOfTwo (const uint uiInput) const
    {
        return uiInput != 0 && (uiInput & (uiInput - 1)) == 0;
    }

    /*
     * method: IsSet
     *
     * Check whether all bits in source bitfield is set on target bitfield.
     *
     * Arguments:
     * uiTarget     - Target bitfield.
     * uiSource     - Source bitfield.
     *
     * Returns:
     * true     - All bits is set.
     * false    - None bit/Few bits is set.
     */
    bool IsSet (const uint uiTarget, const uint uiSource) const
    {
        return (uiTarget & uiSource) == uiSource;
    }

    /*
     * method: Has
     *
     * Check whether one of bit in source bitfield is set on target bitfield.
     *
     * Arguments:
     * uiTarget     - Target bitfield.
     * uiSource     - Source bitfield.
     *
     * Returns:
     * true     - One/Few/All bits is set.
     * false    - None bit is set.
     */
    bool Has (const uint uiTarget, const uint uiSource) const
    {
        return (uiTarget & uiSource) != 0;
    }

    /*
     * method: Get
     *
     * Get the bit with given shift position.
     *
     * Arguments:
     * uiPosition   - Bit-shift position.
     *
     * Returns:
     * uint     - The result bitfield.
     */
    uint Get (const uint uiPosition) const
    {
        return (1 << uiPosition);//(1 << (uiPosition & 31));//uint( pow(2, uiPosition) );
    }

    /*
     * method: Set
     *
     * Set target bitfield with given source bitfield.
     *
     * Arguments:
     * uiTarget     - Target bitfield.
     * uiSource     - Source bitfield.
     *
     * Returns:
     * uint     - The result bitfield.
     */
    uint Set (const uint uiTarget, const uint uiSource) const
    {
        return (uiTarget | uiSource);
    }

    /*
     * method: Unset
     *
     * Unset target bitfield with given source bitfield.
     *
     * Arguments:
     * uiTarget     - Target bitfield.
     * uiSource     - Source bitfield.
     *
     * Returns:
     * uint     - The result bitfield.
     */
    uint Unset (const uint uiTarget, const uint uiSource) const
    {
        return (uiTarget & ~uiSource);
    }

    /*
     * method: CountSet
     *
     * Count the bits set.
     *
     * Arguments:
     * uiBitField   - Input bitfield.
     *
     * Returns:
     * uint     - The bits set count.
     */
    uint CountSet (uint uiBitField) const
    {
        // c accumulates the total bits set in v.
        uint c;
        for (c = 0; uiBitField != 0; c++)
        {
            // clear the least significant bit set.
            uiBitField &= uiBitField - 1;
        }
        return c;
    }

    /*
     * method: FromFlags
     *
     * Parse given AMXx flags string to a bitfield.
     *
     * Arguments:
     * szInput  - The AMXx flags string.
     *
     * Returns:
     * uint     - The bitfield.
     */
    uint FromFlags (string &in szInput) const
    {
        szInput = szInput.ToLowercase();
        szInput.Trim();
        uint uiFlags = 0;
        for (uint i = 0; i < szInput.Length(); i++)
        {
            uiFlags = Set( uiFlags, Get(uint(szInput[i]) - FIRST_ALPHABET) );
        }
        return uiFlags;
    }

    /*
     * method: ToFlags
     *
     * Convert given bitfield to an AMXx flags string.
     *
     * Arguments:
     * uiBitField   - The bitfield.
     *
     * Returns:
     * string   - The AMXx flags string.
     */
    string ToFlags (uint uiBitField) const
    {
        string szResult;
        for (uint i = 0; uiBitField != 0 && i < ALPHABET_COUNT; i++)
        {
            const uint uiBit = Get( i );
            if (IsSet( uiBitField, uiBit ))
            {
                szResult += char( uint(FIRST_ALPHABET) + i );
            }
            uiBitField = Unset( uiBitField, uiBit );
        }
        return szResult;
    }

}

}}
