/*
 * file: libplayer.as
 *
 * Common Player Library.
 *
 * Some methods are taken from Bit Twiddling Hacks By Sean Eron Anderson.
 * <https://graphics.stanford.edu/~seander/bithacks.html>
 */
/*
 * About: MIT License
 *
 * Copyright (c) 2021 Anggara Yama Putra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/*
 * namespace: AN
 *
 * --- C++
 * namespace AN {...}
 * ---
 */
namespace AN{

/*
 * obj: g_LibPlayer
 *
 * Global instance of <AN::LIBPLAYER::CLibPlayer>.
 */
const AN::LIBPLAYER::CLibPlayer g_LibPlayer();

/*
 * namespace: AN::LIBPLAYER
 *
 * --- C++
 * namespace AN::LIBPLAYER {...}
 * ---
 */
namespace LIBPLAYER{

/*
 * string: _VERSION
 */
const string _VERSION = "2021-03-26.0002";

/*
 * obj: g_LibDebug
 *
 * Global instance of <AN::LIBDEBUG::CLibDebug>.
 */
const AN::LIBDEBUG::CLibDebug g_LibDebug( "Common.Player." + AN::LIBDEBUG::_VERSION );

/*
 * class: AN::LIBPLAYER::CLibPlayer
 *
 * --- C++
 * final class AN::LIBPLAYER::CLibPlayer {...}
 * ---
 *
 * Player Library.
 */
final class CLibPlayer
{

    /*
     * constructor: CLibPlayer
     *
     * Default constructor.
     */
    CLibPlayer ()
    {
        if (g_LibModule.IsPlugin())
            g_Hooks.RegisterHook( Hooks::Player::ClientConnected, @m_pClientConnectedHook );
        g_Hooks.RegisterHook( Hooks::Player::ClientPutInServer, @m_pClientPutInServerHook );
        g_Hooks.RegisterHook( Hooks::Player::ClientDisconnect, @m_pClientDisconnectHook );
        g_Hooks.RegisterHook( Hooks::Player::PlayerSpawn, @m_pPlayerSpawnHook );
        g_Hooks.RegisterHook( Hooks::Player::PlayerKilled, @m_pPlayerKilledHook );
        g_Hooks.RegisterHook( Hooks::Player::PlayerEnteredObserver, @m_pPlayerEnteredObserverHook );
    }

    /*
     * destructor: CLibPlayer
     *
     * Default destructor.
     */
    ~CLibPlayer ()
    {
        if (g_LibModule.IsPlugin())
            g_Hooks.RemoveHook( Hooks::Player::ClientConnected, @m_pClientConnectedHook );
        g_Hooks.RemoveHook( Hooks::Player::ClientPutInServer, @m_pClientPutInServerHook );
        g_Hooks.RemoveHook( Hooks::Player::ClientDisconnect, @m_pClientDisconnectHook );
        g_Hooks.RemoveHook( Hooks::Player::PlayerSpawn, @m_pPlayerSpawnHook );
        g_Hooks.RemoveHook( Hooks::Player::PlayerKilled, @m_pPlayerKilledHook );
        g_Hooks.RemoveHook( Hooks::Player::PlayerEnteredObserver, @m_pPlayerEnteredObserverHook );
    }

    /*
     * method: GetMaxPlayer
     *
     * Gets maximum player slots.
     *
     * Returns:
     * uint     - The maximum player slots.
     */
    uint GetMaxPlayer () const
    {
        return uint( g_Engine.maxClients );
    }

    /*
     * method: ClientCommand
     *
     * Sends a SVC_STUFFTEXT network message.
     *
     * Arguments:
     * pPlayer      - Target player (null = all players).
     * szCommand    - Command to send & execute.
     */
    void ClientCommand (CBasePlayer@ pPlayer, const string& in szCommand) const
    {
        ClientCommand( pPlayer !is null ? pPlayer.edict() : null, szCommand );
    }

    /*
     * method: ClientCommand
     *
     * Sends a SVC_STUFFTEXT network message.
     *
     * Arguments:
     * pEdict       - Target player (null = all players).
     * szCommand    - Command to send & execute.
     */
    void ClientCommand (edict_t@ pEdict, const string& in szCommand) const
    {
        NetworkMessage msg( pEdict is null ? MSG_ALL : MSG_ONE, NetworkMessages::SVC_STUFFTEXT, @pEdict );
            msg.WriteString( szCommand );
        msg.End();
    }

    /*
     * method: EnableMouseCursor
     *
     * Enable/Disable player camera mouse cursor.
     * Only usable on trigger_camera view.
     *
     * Arguments:
     * pPlayer      - Target player (null = all players).
     * bEnable      - (true = Enable)(false = Disable).
     */
    void EnableMouseCursor (CBasePlayer@ pPlayer, const bool bEnable) const
    {
        EnableMouseCursor( pPlayer !is null ? pPlayer.edict() : null, bEnable );
    }

    /*
     * method: EnableMouseCursor
     *
     * Enable/Disable player camera mouse cursor.
     * Only usable on trigger_camera view.
     *
     * Arguments:
     * pEdict       - Target player (null = all players).
     * bEnable      - (true = Enable)(false = Disable).
     */
    void EnableMouseCursor (edict_t@ pEdict, const bool bEnable) const
    {
        NetworkMessage msg( pEdict is null ? MSG_ALL : MSG_ONE, NetworkMessages::CameraMouse, @pEdict );
            msg.WriteByte( bEnable ? 1 : 0 );
        msg.End();
    }

    /*
     * method: IsBot
     *
     * Check whether this player is a bot.
     *
     * Arguments:
     * pPlayer      - Target player.
     *
     * Returns:
     * true     - The player is a fake client.
     * false    - The player is null.
     *            / The player is a legit client.
     */
    bool IsBot (CBasePlayer@ pPlayer) const
    {
        if (pPlayer is null) return false;
        return IsBot( pPlayer.edict() );
    }

    /*
     * method: IsBot
     *
     * Check whether this player is a bot.
     *
     * Arguments:
     * pEdict       - Target player.
     *
     * Returns:
     * true     - The player is a fake client.
     * false    - The player is null.
     *            / The player is a legit client.
     */
    bool IsBot (edict_t@ pEdict) const
    {
        if (pEdict is null) return false;

        if (g_LibBit.IsSet(pEdict.vars.flags, FL_FAKECLIENT))
            return true;

        const string szAuthId = g_EngineFuncs.GetPlayerAuthId( @pEdict );
        if (szAuthId == "BOT")
            return true;

        return false;
    }

    /*
     * method: IsConnected
     *
     * Check whether given player is connected.
     *
     * Arguments:
     * pPlayer      - Target player.
     *
     * Returns:
     * true     - The player is connected to the server.
     * false    - The player is null.
     *            / The player is disconnected from the server.
     */
    bool IsConnected (CBasePlayer@ pPlayer) const
    {
        if (pPlayer is null) return false;
        return IsConnected( pPlayer.edict() );
    }

    /*
     * method: IsConnected
     *
     * Check whether given player is connected.
     *
     * Arguments:
     * pEdict       - Target player.
     *
     * Returns:
     * true     - The player is connected to the server.
     * false    - The player is null.
     *            / The player is disconnected from the server.
     */
    bool IsConnected (edict_t@ pEdict) const
    {
        if (pEdict is null) return false;
        return g_LibBit.IsSet( m_uiConnectedBitField, g_LibBit.Get(g_EngineFuncs.IndexOfEdict(@pEdict)) );
    }

    /*
     * method: IsAlive
     *
     * Check whether given player is alive.
     *
     * Arguments:
     * pPlayer      - Target player.
     *
     * Returns:
     * true     - The player is alive.
     * false    - The player is null.
     *            / The player is dead.
     */
    bool IsAlive (CBasePlayer@ pPlayer) const
    {
        if (pPlayer is null) return false;
        return IsAlive( pPlayer.edict() );
    }

    /*
     * method: IsAlive
     *
     * Check whether given player is alive.
     *
     * Arguments:
     * pEdict       - Target player.
     *
     * Returns:
     * true     - The player is alive.
     * false    - The player is null.
     *            / The player is dead.
     */
    bool IsAlive (edict_t@ pEdict) const
    {
        if (pEdict is null) return false;
        return g_LibBit.IsSet( m_uiAliveBitField, g_LibBit.Get(g_EngineFuncs.IndexOfEdict(@pEdict)) );
    }

    /*
     * method: IsJoined
     *
     * Check whether given player is joined the game.
     *
     * Arguments:
     * pPlayer      - Target player.
     *
     * Returns:
     * true     - The player is joined the game.
     * false    - The player is null.
     *            / The player is joined the game.
     */
    bool IsJoined (CBasePlayer@ pPlayer) const
    {
        if (pPlayer is null) return false;
        return IsJoined( pPlayer.edict() );
    }

    /*
     * method: IsJoined
     *
     * Check whether given player is joined the game.
     *
     * Arguments:
     * pEdict       - Target player.
     *
     * Returns:
     * true     - The player is joined the game.
     * false    - The player is null.
     *            / The player is joined the game.
     */
    bool IsJoined (edict_t@ pEdict) const
    {
        if (pEdict is null) return false;
        return g_LibBit.IsSet( m_uiJoinedBitField, g_LibBit.Get(g_EngineFuncs.IndexOfEdict(@pEdict)) );
    }

    /*
     * method: GetAlive
     *
     * Get all alive player entity handles.
     *
     * Returns:
     * array<EHandle>   - Array of alive player entity handles.
     */
    array<EHandle> GetAlive () const
    {
        auto rgPlayer = array<EHandle>();
        uint uiBitField = m_uiAliveBitField;
        for (uint i = 0; uiBitField != 0 && i < GetMaxPlayer(); i++)
        {
            const uint uiBit = g_LibBit.Get( i );
            if (g_LibBit.IsSet( uiBitField, uiBit ))
            {
                rgPlayer.insertLast( EHandle(g_PlayerFuncs.FindPlayerByIndex(i+1)) );
            }
            uiBitField = g_LibBit.Unset( uiBitField, uiBit );
        }
        return rgPlayer;
    }

    /*
     * method: GetDead
     *
     * Get all dead player entity handles.
     *
     * Returns:
     * array<EHandle>   - Array of dead player entity handles.
     */
    array<EHandle> GetDead () const
    {
        auto rgPlayer = array<EHandle>();
        uint uiBitField = m_uiAliveBitField;
        for (uint i = 0; uiBitField != 0 && i < GetMaxPlayer(); i++)
        {
            const uint uiBit = g_LibBit.Get( i );
            if (!g_LibBit.IsSet( uiBitField, uiBit ))
            {
                rgPlayer.insertLast( EHandle(g_PlayerFuncs.FindPlayerByIndex(i+1)) );
            }
            uiBitField = g_LibBit.Unset( uiBitField, uiBit );
        }
        return rgPlayer;
    }

    /*
     * method: GetJoined
     *
     * Get all joined player entity handles.
     *
     * Returns:
     * array<EHandle>   - Array of joined player entity handles.
     */
    array<EHandle> GetJoined () const
    {
        auto rgPlayer = array<EHandle>();
        uint uiBitField = m_uiJoinedBitField;
        for (uint i = 0; uiBitField != 0 && i < GetMaxPlayer(); i++)
        {
            const uint uiBit = g_LibBit.Get( i );
            if (g_LibBit.IsSet( uiBitField, uiBit ))
            {
                rgPlayer.insertLast( EHandle(g_PlayerFuncs.FindPlayerByIndex(i+1)) );
            }
            uiBitField = g_LibBit.Unset( uiBitField, uiBit );
        }
        return rgPlayer;
    }

    /*
     * method: CountConnected
     *
     * Count all connected players.
     *
     * Returns:
     * uint     - Connected player count.
     */
    uint CountConnected () const
    {
        return g_LibBit.CountSet( m_uiConnectedBitField );
    }

    /*
     * method: CountDisconnected
     *
     * Count empty player slots.
     *
     * Returns:
     * uint     - Disconnected player count.
     */
    uint CountDisconnected () const
    {
        return (GetMaxPlayer() - CountConnected());
    }

    /*
     * method: CountAlive
     *
     * Count all alive players.
     *
     * Returns:
     * uint     - Alive player count.
     */
    uint CountAlive () const
    {
        return g_LibBit.CountSet( m_uiAliveBitField );
    }

    /*
     * method: CountDead
     *
     * Count all dead players.
     *
     * Returns:
     * uint     - Dead player count.
     */
    uint CountDead () const
    {
        return (CountConnected() - CountAlive());
    }

    /*
     * method: CountJoined
     *
     * Count all joined players.
     *
     * Returns:
     * uint     - Joined player count.
     */
    uint CountJoined () const
    {
        return g_LibBit.CountSet( m_uiJoinedBitField );
    }

    /*
     * method: OnClientConnected
     *
     * ClientConnected hook.
     */
    private HookReturnCode OnClientConnected (edict_t@ pEdict, const string& in szPlayerName, const string& in szIPAddress, bool& out bDisallowJoin, string& out szRejectReason)
    {
        const auto uiBitFlag = g_LibBit.Get( g_EngineFuncs.IndexOfEdict( @pEdict ) );
        m_uiConnectedBitField   = g_LibBit.Set( m_uiConnectedBitField, uiBitFlag );
        m_uiAliveBitField       = g_LibBit.Unset( m_uiAliveBitField, uiBitFlag );
        m_uiJoinedBitField      = g_LibBit.Unset( m_uiJoinedBitField, uiBitFlag );
        return HOOK_CONTINUE;
    }

    /*
     * method: OnClientPutInServer
     *
     * ClientPutInServer hook.
     */
    private HookReturnCode OnClientPutInServer (CBasePlayer@ pPlayer)
    {
        const auto uiBitFlag = g_LibBit.Get( pPlayer.entindex() );
        if (!g_LibModule.IsPlugin())
            m_uiConnectedBitField = g_LibBit.Set( m_uiConnectedBitField, uiBitFlag );
        m_uiJoinedBitField = g_LibBit.Set( m_uiJoinedBitField, uiBitFlag );
        return HOOK_CONTINUE;
    }

    /*
     * method: OnClientDisconnect
     *
     * ClientDisconnect hook.
     */
    private HookReturnCode OnClientDisconnect (CBasePlayer@ pPlayer)
    {
        const auto uiBitFlag = g_LibBit.Get( pPlayer.entindex() );
        m_uiConnectedBitField   = g_LibBit.Unset( m_uiConnectedBitField, uiBitFlag );
        m_uiAliveBitField       = g_LibBit.Unset( m_uiAliveBitField, uiBitFlag );
        m_uiJoinedBitField      = g_LibBit.Unset( m_uiJoinedBitField, uiBitFlag );
        return HOOK_CONTINUE;
    }

    /*
     * method: OnPlayerSpawn
     *
     * PlayerSpawn hook.
     */
    private HookReturnCode OnPlayerSpawn (CBasePlayer@ pPlayer)
    {
        m_uiAliveBitField |= g_LibBit.Get( pPlayer.entindex() );
        return HOOK_CONTINUE;
    }

    /*
     * method: OnPlayerKilled
     *
     * PlayerKilled hook.
     */
    private HookReturnCode OnPlayerKilled (CBasePlayer@ pPlayer, CBaseEntity@ pAttacker, int iGib)
    {
        m_uiAliveBitField &= ~g_LibBit.Get( pPlayer.entindex() );
        return HOOK_CONTINUE;
    }

    /*
     * method: OnPlayerEnteredObserver
     *
     * PlayerEnteredObserver hook.
     */
    private HookReturnCode OnPlayerEnteredObserver (CBasePlayer@ pPlayer)
    {
        const auto uiBitFlag    = g_LibBit.Get( pPlayer.entindex() );
        m_uiAliveBitField       = g_LibBit.Unset( m_uiAliveBitField, uiBitFlag );
        return HOOK_CONTINUE;
    }

    /*
     * uint: m_uiConnectedBitField
     */
    private uint                    m_uiConnectedBitField   = 0;

    /*
     * uint: m_uiJoinedBitField
     */
    private uint                    m_uiJoinedBitField      = 0;

    /*
     * uint: m_uiAliveBitField
     */
    private uint                    m_uiAliveBitField       = 0;

    /*
     * ptr: m_pClientConnectedHook
     */
    private ClientConnectedHook@    m_pClientConnectedHook    = ClientConnectedHook(this.OnClientConnected);

    /*
     * ptr: m_pClientPutInServerHook
     */
    private ClientPutInServerHook@  m_pClientPutInServerHook  = ClientPutInServerHook(this.OnClientPutInServer);

    /*
     * ptr: m_pClientDisconnectHook
     */
    private ClientDisconnectHook@   m_pClientDisconnectHook   = ClientDisconnectHook(this.OnClientDisconnect);

    /*
     * ptr: m_pPlayerSpawnHook
     */
    private PlayerSpawnHook@        m_pPlayerSpawnHook        = PlayerSpawnHook(this.OnPlayerSpawn);

    /*
     * ptr: m_pPlayerKilledHook
     */
    private PlayerKilledHook@       m_pPlayerKilledHook       = PlayerKilledHook(this.OnPlayerKilled);

    /*
     * ptr: m_pPlayerEnteredObserverHook
     */
    private PlayerEnteredObserverHook@ m_pPlayerEnteredObserverHook = PlayerEnteredObserverHook(this.OnPlayerEnteredObserver);

}

}}
