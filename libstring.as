/*
 * file: libstring.as
 *
 * Common String Library.
 *
 * Some methods are taken from Bit Twiddling Hacks By Sean Eron Anderson.
 * <https://graphics.stanford.edu/~seander/bithacks.html>
 */
/*
 * About: MIT License
 *
 * Copyright (c) 2021 Anggara Yama Putra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/*
 * namespace: AN
 *
 * --- C++
 * namespace AN {...}
 * ---
 */
namespace AN{

/*
 * obj: g_LibString
 *
 * Global instance of <AN::LIBSTRING::CLibString>.
 */
const AN::LIBSTRING::CLibString g_LibString();

/*
 * namespace: AN::LIBSTRING
 *
 * --- C++
 * namespace AN::LIBSTRING {...}
 * ---
 */
namespace LIBSTRING{

/*
 * string: _VERSION
 */
const string _VERSION = "2021-03-26.0001";

/*
 * obj: g_LibDebug
 *
 * Global instance of <AN::LIBDEBUG::CLibDebug>.
 */
const AN::LIBDEBUG::CLibDebug g_LibDebug( "Common.String." + AN::LIBDEBUG::_VERSION );

/*
 * obj: INTEGER_REGEX
 *
 * <AN::LIBSTRING::CLibString::IsInteger> regex.
 */
const Regex::Regex INTEGER_REGEX( '^[-]?\\d+$', Regex::FlagType(Regex::ECMAScript | Regex::icase) );

/*
 * class: AN::LIBSTRING::CLibString
 *
 * --- C++
 * final class AN::LIBSTRING::CLibString {...}
 * ---
 *
 * String Library.
 */
final class CLibString
{

    /*
     * constructor: CLibString
     *
     * Default constructor.
     */
    CLibString ()
    {
    }

    /*
     * destructor: CLibString
     *
     * Default destructor.
     */
    ~CLibString ()
    {
    }

    /*
     * method: IsInteger
     *
     * Check whether this string is integer only.
     *
     * Arguments:
     * szInput      - Input string.
     *
     * Returns:
     * true     - Value is an integer.
     * false    - Value is not an integer.
     */
    bool IsInteger (const string &in szInput) const
    {
        return Regex::Match( szInput, INTEGER_REGEX );
    }

    /*
     * method: GetFormattedString
     *
     * Format the string with given array of string arguments.
     *
     * Arguments:
     * szMessage        - String to format with.
     * szArgs           - The array of string arguments.
     *
     * Returns:
     * string   - The result string.
     */
    string GetFormattedString (const string &in szMessage, const array<string> &in szArgs) const
    {
        string szResult;
        switch( szArgs.length() )
        {
            case 0:
            {
                szResult = szMessage;
                break;
            }
            case 1:
            {
                snprintf( szResult, szMessage, szArgs[0] );
                break;
            }
            case 2:
            {
                snprintf( szResult, szMessage, szArgs[0], szArgs[1] );
                break;
            }
            case 3:
            {
                snprintf( szResult, szMessage, szArgs[0], szArgs[1], szArgs[2] );
                break;
            }
            case 4:
            {
                snprintf( szResult, szMessage, szArgs[0], szArgs[1], szArgs[2], szArgs[3] );
                break;
            }
            case 5:
            {
                snprintf( szResult, szMessage, szArgs[0], szArgs[1], szArgs[2], szArgs[3], szArgs[4] );
                break;
            }
            case 6:
            {
                snprintf( szResult, szMessage, szArgs[0], szArgs[1], szArgs[2], szArgs[3], szArgs[4], szArgs[5] );
                break;
            }
            case 7:
            {
                snprintf( szResult, szMessage, szArgs[0], szArgs[1], szArgs[2], szArgs[3], szArgs[4], szArgs[5], szArgs[6] );
                break;
            }
            case 8:
            default:
            {
                snprintf( szResult, szMessage, szArgs[0], szArgs[1], szArgs[2], szArgs[3], szArgs[4], szArgs[5], szArgs[6], szArgs[7] );
                break;
            }
        }
        return szResult;
    }

}

}}
