/*
 * file: libfile.as
 *
 * Common File Library.
 */
/*
 * About: MIT License
 *
 * Copyright (c) 2021 Anggara Yama Putra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/*
 * namespace: AN
 *
 * --- C++
 * namespace AN {...}
 * ---
 */
namespace AN{

/*
 * obj: g_LibFile
 *
 * Global instance of <AN::LIBFILE::CLibFile>.
 */
const AN::LIBFILE::CLibFile g_LibFile();

/*
 * namespace: AN::LIBFILE
 *
 * --- C++
 * namespace AN::LIBFILE {...}
 * ---
 */
namespace LIBFILE{

/*
 * string: _VERSION
 */
const string _VERSION = "2021-03-24.0001";

/*
 * obj: g_LibDebug
 *
 * Global instance of <AN::LIBDEBUG::CLibDebug>.
 */
const AN::LIBDEBUG::CLibDebug g_LibDebug( "Common.File." + AN::LIBDEBUG::_VERSION );

/*
 * class: AN::LIBFILE::CLibFile
 *
 * --- C++
 * final class AN::LIBFILE::CLibFile {...}
 * ---
 *
 * File library.
 */
final class CLibFile
{

    /*
     * constructor: CLibFile
     *
     * Default constructor.
     */
    CLibFile ()
    {
    }

    /*
     * method: IsExists
     *
     * Check whether the file is exists.
     *
     * Arguments:
     * szFilePath   - File path.
     *
     * Returns:
     * true         - File is exists
     * false        - Can't open the file.
     */
    bool IsExists (const string &in szFilePath) const
    {
        auto@ pFile = g_FileSystem.OpenFile( szFilePath, OpenFile::OpenFile(OpenFile::READ | OpenFile::BINARY) );
        if (pFile is null)
            return false;

        if (!pFile.IsOpen())
            return false;

        pFile.ReadCharacter();
        const bool bResult = pFile.EOFReached() || pFile.Tell() >= 0;
        pFile.Close();
        return bResult;
    }

    /*
     * method: ReadFile
     *
     * Open a file with read access.
     *
     * Arguments:
     * szFilePath   - File path.
     * bAsBinary    - Read as binary file.
     *
     * Returns:
     * File@        - File instance pointer.
     * null         - Can't open the file.
     */
    File@ ReadFile (const string &in szFilePath, const bool bAsBinary = false) const
    {
        uint uiFlags = OpenFile::READ;

        if (bAsBinary)
            uiFlags |= OpenFile::BINARY;

        return g_FileSystem.OpenFile( szFilePath, OpenFile::OpenFile(uiFlags) );
    }

    /*
     * method: GetFileExtension
     *
     * Get the file extension with given path.
     *
     * Arguments:
     * szFilePath   - File path.
     *
     * Returns:
     * Non-empty string - The file extension.
     * Empty string     - Failed to parse given path.
     */
    string GetFileExtension (const string &in szFilePath) const
    {
        uint i;
        for (i = szFilePath.Length()-1; (i >= 0) && (szFilePath[i] != '/') && (szFilePath[i] != '\\'); i--)
        {
            if (szFilePath[i] == '.')
            {
                string szResult = szFilePath.SubString( ++i );
                szResult.Trim();
                return szResult;
            }
        }
        return string();
    }

}

}}
